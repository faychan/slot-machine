// Import Application class that is the main part of our PIXI project
import { Application } from '@pixi/app'

// In order that PIXI could render things we need to register appropriate plugins
import { Renderer } from '@pixi/core' // Renderer is the class that is going to register plugins

import { BatchRenderer } from '@pixi/core' // BatchRenderer is the "plugin" for drawing sprites
Renderer.registerPlugin('batch', BatchRenderer)

import { TickerPlugin } from '@pixi/ticker' // TickerPlugin is the plugin for running an update loop (it's for the application class)
Application.registerPlugin(TickerPlugin)

// And just for convenience let's register Loader plugin in order to use it right from Application instance like app.loader.add(..) etc.
import { AppLoaderPlugin } from '@pixi/loaders'
Application.registerPlugin(AppLoaderPlugin)

// Sprite is our image on the stage
import { Sprite } from '@pixi/sprite'

import * as PIXI from "pixi.js";
global.PIXI = PIXI;
require('pixi.js');

var renderer = PIXI.autoDetectRenderer(
  800, 600,
  {antialiasing: false, transparent: true, reslution: devicePixelRatio, autoResize: true}  
);

renderer.backgroundColor = '0xFFFFFF';
document.body.appendChild(renderer.view);
var stage = new PIXI.Container();
stage.interactive=true;

renderer.view.style.position = 'absolute';
renderer.view.style.left = '50%';
renderer.view.style.top = '50%';
renderer.view.style.transform = 'translate3d( -50%, -50%, 0 )';

// App with width and height of the page
var STATE_ZERO = 0;
var STATE_INIT=1;
var STATE_MOVING=2;
var STATE_CHECK_WIN=3;
    
var SLOT_NUMBER = 3;
var INITIAL_X = (renderer.screen.width/2) - 170;
var TILE_HEIGHT = 100;
var TILE_WIDTH = 102;
var N_CYCLE = 5;
var TOT_TILES= 7;
    
/*
* 0: fermo
* 1: moving
* 2: check win
*/
var gameStatus=0;
var finalTileY=[];
var slotSprite=[];
var preChoosedPosition = [];

//loading images
PIXI.Loader.shared
	.add("IMG_MACHINE", "./assets/slot-machine.png")
	.add("IMG_BODY", "./assets/body.png")
	.add("IMG_BUTTON", "./assets/button.png")

	.load(setup)

//setup
function setup() {
	var bodySprite = new PIXI.Sprite.from('IMG_BODY');
  bodySprite.x=renderer.screen.width / 2;
	bodySprite.y=renderer.screen.height / 2;
	bodySprite.anchor.set(0.5);
	bodySprite
  stage.addChild(bodySprite);
  var buttonSprite = new PIXI.Sprite.from('IMG_BUTTON');
  buttonSprite.x=592;
  buttonSprite.y=180;
  stage.addChild(buttonSprite);
	buttonSprite.interactive=true;	
	buttonSprite.click = function (e) {
    startAnimation();
	}
  
	//tiles
  var texture=PIXI.Texture.from('IMG_MACHINE');
  preChoosedPosition = [1,2,3];
  for(var i=0; i<SLOT_NUMBER; i++) {
		slotSprite[i] = new PIXI.TilingSprite(texture, TILE_WIDTH, TILE_HEIGHT+20);
		slotSprite[i].tilePosition.x = 0;
		slotSprite[i].tilePosition.y = (-preChoosedPosition[i]*TILE_HEIGHT)+10;
		slotSprite[i].x= INITIAL_X +(i*118);
		slotSprite[i].y= (renderer.screen.height/2 + 12);
		stage.addChild( slotSprite[i] );
	}
	draw();

	function animateToggle() {
		buttonSprite.scale.set(1,0.5);
		buttonSprite.y=280;
		setTimeout(() => { 
			buttonSprite.scale.set(1,1);
			buttonSprite.y=180;
		}, 50);
	}
	function startAnimation() {
		animateToggle();
		if( gameStatus==STATE_INIT || gameStatus==STATE_CHECK_WIN ) {
			preChoosedPosition = getRandomPositions();
			for(var i=0; i<SLOT_NUMBER; i++) {
				preChoosedPosition[i] = getRandomInt(0,6);
				slotSprite[i].tilePosition.y = (-preChoosedPosition[i]*TILE_HEIGHT) +10;
				finalTileY[i]= (N_CYCLE*TILE_HEIGHT*TOT_TILES);
			}
			gameStatus = STATE_MOVING;
			draw();
		}   
	}
}

var INC = [ 15,20,25 ];

//functions draw
function draw() {
	if(gameStatus==STATE_ZERO) {
			gameStatus=STATE_INIT;
	} else 
if(gameStatus==STATE_INIT) {
		gameStatus=STATE_CHECK_WIN;
		
} else if(gameStatus==STATE_MOVING) {
	 
	for(var i=0; i<SLOT_NUMBER; i++) {
		if( finalTileY[i] > 0 ) {
			slotSprite[i].tilePosition.y = slotSprite[i].tilePosition.y + INC[i];
			finalTileY[i]= finalTileY[i] - INC[i];
		}            
	}
	
	if( finalTileY[0]-5 <= 0 ) {
		gameStatus=STATE_CHECK_WIN;
	}
		
} else if(gameStatus==STATE_CHECK_WIN) {
	var test=true;
	for(var i=1; i<SLOT_NUMBER; i++) {
		if( preChoosedPosition[i]!=preChoosedPosition[i-1]) {
			test=false;
		}
	}
	if(test) {
		alert("Congratulations, you won!");   
	}
		return; //no more animation
	}
	renderer.render(stage);
	requestAnimationFrame(draw);
}
    
    
/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
    
function getRandomPositions() {
    var x = getRandomInt(0, 100);
    if(x>50) {
        x= getRandomInt(0,6);
        return [x,x,x];
    }
    return [getRandomInt(0,6),getRandomInt(0,6),getRandomInt(0,6)];   
}